module bitbucket.org/tiluvy/shippy-cli-user

go 1.12

replace bitbucket.org/tiluvy/shippy-service-user => ../shippy-service-user

require bitbucket.org/tiluvy/shippy-service-user v0.0.0-00010101000000-000000000000 // indirect
